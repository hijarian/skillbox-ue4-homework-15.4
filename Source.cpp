#include <iostream>

bool is_even(const unsigned int& number)
{
	return number % 2 == 0; // ������, ��� ���� - ������.
}

void print_divisible_numbers(const unsigned int& max_number, bool divisibility_checker(const unsigned int&) )
{
	for (unsigned int i = 0; i <= max_number; ++i)
	{
		if (divisibility_checker(i))
		{
			std::cout << i << " ";
		}
	}
	std::cout << std::endl;
}

int main()
{
	const unsigned int N = 42;

	for (unsigned int i = 0; i <= N; ++i)
	{
		if (is_even(i))
		{
			std::cout << i << " ";
		}
	}
	std::cout << std::endl;

	print_divisible_numbers(42, &is_even);
	print_divisible_numbers(42, [](const unsigned int& number) { return !is_even(number);  });
}